﻿using Microsoft.Win32.SafeHandles;
using System.ComponentModel.Design;
using System.Text.RegularExpressions;

Console.OutputEncoding = System.Text.Encoding.UTF8;



//menu danh sach
void Menu()
{
    Console.Write("----- Menu ------\n"
                    + "1. Xem danh sach so da duoc luu trong mang\n"
                    + "2. Them so moi\n"
                    + "3. Xoa 1 so\n"
                    + "4. thoat\n"
                    + "Moi nhap lua chon: ");
}




// ham check ngoai le 
void Check(int input)
{
    while (input < 1 || input > 4)
    {
        Console.WriteLine("So ban nhap khong hop le.");
        Console.Write("Moi ban nhap lai: ");
        input = int.Parse(Console.ReadLine());
    }
}


// ham lua chon cac option phu hop

int[] danhsach = new int[0];

void Option(int option)
{
    Check(option);
    while (option != 4)
    {
        switch (option)
        {
            case 1:
                if (danhsach.Length == 0)
                {
                    Console.WriteLine("Hien tai ban chua lu so dac biet vao danh sach");
                }
                else
                {
                    PrintLottery();
                }
                break;
            case 2:// case 2 luu tru so 
                Array.Resize(ref danhsach, danhsach.Length + 1);

                //pattern Lottery 6 (đảm bảo là 6 ký tự số, bắt đầu bằng số và kéo dài đúng 6 ký tự)
                Regex reg = new Regex(@"^\d{6}$");
                // nhằm lưu lại kết quả của regex
                Match verify;


                Console.Write("Moi ban nhap so co 6 chu so: ");
                // nếu không match bắt nhập lại
                do
                {
                    //người dùng nhập vào
                    string LotteryString = Console.ReadLine();

                    //lưu kết quả của việc so sánh
                    verify = reg.Match(LotteryString);

                    // nếu mà chưa đúng thì in ra thông báo
                    if (verify.Success is false)
                    {
                        Console.WriteLine("So ban nhap khong hop le.");
                        Console.WriteLine("Vui long nhap so co 6 chu so");
                        Console.Write("Moi ban nhap so co 6 chu so: ");
                    }
                    else
                    {
                        // nếu true thì gán giá trị cho phần tử cuối cùng của mảng hiện tại
                        danhsach[danhsach.Length - 1] = int.Parse(LotteryString);
                    }

                } while (verify.Success is false); // nếu chưa đúng thì loop 

                break;
            case 3:
                bool isLoop = false;
                do
                {
                    if (danhsach.Length == 0)
                    {
                        Console.WriteLine("Danh sach hien tai dang trong");
                        //danh sách trống thì break luôn;
                        break;
                    }
                    else
                    {
                        // show ra danh sach truoc khi xoa (nhớ là danh sách này mình sẽ hiện từ 1 nên index phải cộng lên 1)
                        PrintLottery();
                        // Index của phần tử cần xóa, người dùng chọn số và mình phải trừ đi 1;

                        Console.WriteLine("Nhập vị trí số cần xóa : ");

                        //index trên screen
                        int indexToRemoveInScreen;

                        // kiểm tra xem có đúng là số không ?, và nó giúp cho app sẽ không bị ngừng nếu như người dùng nhập bậy 
                        bool isNumber = int.TryParse(Console.ReadLine(), out indexToRemoveInScreen);

                        // đây mới là index thực sự cần xóa nhé
                        --indexToRemoveInScreen;
                        //remove index
                        if (isNumber && indexToRemoveInScreen >= 0 && indexToRemoveInScreen < danhsach.Length) // 3 điều kiện này thì easy rồi
                        {
                            // đúng thì call func remove đồng thời break vòng lặp ;
                            RemoveIndex(indexToRemoveInScreen);
                            isLoop = true;
                            Console.WriteLine($"index thực sự xóa trong mảng : {indexToRemoveInScreen}");
                        }
                    }
                } while (!isLoop);

                break;
            case 4:
                break;
            default:
                Check(option);
                break;
        }
        Menu();
        option = int.Parse(Console.ReadLine());
    }
}


//func print lottery
void PrintLottery()
{
    if (danhsach.Length == 0)
    {
        Console.WriteLine("danh sách trống");

    }
    else
    {
        Console.WriteLine("danh sách các số :");

        for (int i = 0; i < danhsach.Length; i++)
        {
            Console.WriteLine(i + 1 + ". " + danhsach[i]);
        }
    }
    
}


// remove theo index
void RemoveIndex(int indexToRemove)
{
    Console.WriteLine($"{indexToRemove}");
    // Tạo mảng mới bằng cách loại bỏ phần tử tại indexToRemove
    int[] newArray = new int[danhsach.Length - 1];
    for (int i = 0, j = 0; i < danhsach.Length; i++)
    {
        if (i != indexToRemove)
        {
            newArray[j] = danhsach[i];
            j++;
        }
    }

    //sau khi xóa xong thì gán lại cho mảng ban đầu
    danhsach = newArray;
    // In ra mảng sau khi xóa
    PrintLottery();
}

Menu();
int option = int.Parse(Console.ReadLine());
Option(option);
Console.WriteLine("Ban da thoat");